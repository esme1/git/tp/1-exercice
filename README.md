# Bash script

In this excercise, you will create a bash script. You can have a look at [bash cheat sheet](https://devhints.io/bash) if you need help
You should track all you work thanks to git. Each chapter should be at leat one commit on master branch

## hello world

Print hello world to the screen

## Basic command

print to the user in wich directory the script has been execute
print the name of the user

## Function

create a function that ask the age of the user then calculate his year of birth

## Command Line Interface (CLI)

make your script allow argument:
--help
	This argument print help message to explain what does your script
